using Challenge.Printer.Models;
using Challenge.Printer.Services;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Tests
{
    public class Tests
    {
        public InvoiceService InvoiceService { get; set; }
        public SalesTaxCalculator SalesTaxCalculator { get; set; }
        public MarginAmountCalculator MarginAmountCalculator { get; set; }


        [SetUp]
        public void OneTimeSetup()
        {
            //Could have used Mock. Fakes were easier since because of the simplity of services
            InvoiceService = new InvoiceService();
            SalesTaxCalculator = new SalesTaxCalculator();
            MarginAmountCalculator = new MarginAmountCalculator();
        }

        [Test]
        public void IfLoadedEmptyJob_AnExceptionShouldBeThrown()
        {    
            Assert.Throws(typeof(Exception), new TestDelegate(TestException), "The job is empty. Please enter job items");
        }


        [Test, TestCaseSource(typeof(TestCasePrinterJobs), "TestCases")]
        public string TestPrintJob(List<IPrintItem> items, bool isExtraMargin)
        {
            var printJob = new PrinterJob(items, isExtraMargin? 0.05m: 0.00m);

            var printerService = new PrinterService(InvoiceService, SalesTaxCalculator, MarginAmountCalculator);
            printerService.LoadJob(printJob);

            printerService.Process();

           return printerService.PrintInvoice();
        }

        public void TestException()
        {
            var printJob = new PrinterJob(new List<IPrintItem>(), 0.00m);
            var printerService = new PrinterService(InvoiceService, SalesTaxCalculator, MarginAmountCalculator);
            printerService.LoadJob(printJob);
        }


        public class TestCasePrinterJobs
        {
            public static IEnumerable TestCases
            {
                get
                {
                    ////job 1
                    var printItemsJob1 = new List<IPrintItem> { new PrintItem { Name = "envelopes", Amount = 520.00m, IsExempt = false },
                                                                new PrintItem { Name = "letterhead", Amount = 1983.37m, IsExempt = true} };

                    var testCaseDataJob1 = new TestCaseData(printItemsJob1, true);  //is extraMargin
                    var job1OutPutData = new StringBuilder()
                                          .AppendLine("envelopes: $556.40")
                                          .AppendLine("letterhead: $1983.37")
                                          .AppendLine("total: $2940.30");

                    testCaseDataJob1.Returns(job1OutPutData.ToString());

                    yield return testCaseDataJob1.SetName("Job 1: anvelopes and letterhead");

                    //job 2:
                    var printItemsJob2 = new List<IPrintItem> { new PrintItem { Name = "t-shirts", Amount = 294.04m, IsExempt = false } };
                    var testCaseDataJob2 = new TestCaseData(printItemsJob2, false);  //is extraMargin
                    var job2OutPutData = new StringBuilder()
                                          .AppendLine("t-shirts: $314.62")
                                          .AppendLine("total: $346.96");
                    testCaseDataJob2.Returns(job2OutPutData.ToString());

                    yield return testCaseDataJob2.SetName("Job 2: t-shirts");


                    //job 3
                    var printItemsJob3 = new List<IPrintItem> { new PrintItem { Name = "frisbees", Amount = 19385.38m, IsExempt = true },
                                                                new PrintItem {  Name = "yo-yos", Amount = 1829, IsExempt = true} };

                    var testCaseDataJob3 = new TestCaseData(printItemsJob3, true);  //is extraMargin
                    var job3OutPutData = new StringBuilder()
                                            .AppendLine("frisbees: $19385.38")
                                            .AppendLine("yo-yos: $1829.00")
                                            .AppendLine("total: $24608.68");

                    testCaseDataJob3.Returns(job3OutPutData.ToString());

                    yield return testCaseDataJob3.SetName("Job 3: frisbees and yo-yos");

                }
            }
        }
    }
}