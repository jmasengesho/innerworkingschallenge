﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.Printer.Models
{
    public class PrinterJobDTO
    {
        public string JobId { get; set; }
        public bool IsExtraMargin { get; set; }
        public List<PrintItemDTO> Items { get; set; }
    }
}
