﻿using Challenge.Printer.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.Printer.Models
{
    public class PrinterJob : IPrinterJob
    {
        public List<IPrintItem> PrintItems { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal ExtraMarginRatio { get; set; }
        public decimal MarginAmount { get; set; }
        public bool IsExtraMargin { get; set; }
        public string JobId { get; set; }  //or name, used in printing to distinguish tasks

        public PrinterJob(List<IPrintItem> items, decimal extraMargin = 0.00m)
        {
            PrintItems = items;
            ExtraMarginRatio = extraMargin;
        }

        public PrinterJob(PrinterJobDTO job)
        {
            PrintItems = new List<IPrintItem>();
            IsExtraMargin = job.IsExtraMargin;
            JobId = job.JobId;

            //check if the job is extra margin
            if (IsExtraMargin)
            {
                ExtraMarginRatio = 0.05m;
            }
            //loop through all items
            foreach (var item in job.Items)
            {
                PrintItems.Add(new PrintItem(item));
            }
        }
    }
}
