﻿using System.Collections.Generic;

namespace Challenge.Printer.Models
{
    public interface IPrinterJob
    {
        List<IPrintItem> PrintItems { set; get; }
        string JobId { get; set; }
        decimal TotalAmount { get; set; }
        decimal MarginAmount { get; set; }
        decimal ExtraMarginRatio { get; set; }
    }
}
