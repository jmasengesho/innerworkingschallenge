﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.Printer.Models
{
    public interface IPrintItem
    {
        string Name { get; set; }
        bool IsExempt { get; set; }
        decimal Amount { get; set; }
        decimal SalesTaxAmount { get; set; }
        decimal MarginAmount { get; set; }
        decimal TotalAmount { get; set; }
        decimal CalculateTotalAmount();
    }
}
