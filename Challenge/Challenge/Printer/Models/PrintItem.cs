﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.Printer.Models
{
    public class PrintItem : IPrintItem
    {
        public string Name { get; set; }
        public decimal Amount { get; set; }  //original amount
        public decimal SalesTaxAmount { get; set; } //so we can track the sales tax dollars
        public decimal TotalAmount { get; set; } 
        public bool IsExempt { get; set; }
        public decimal MarginAmount { get; set; } //so we can track margin dollars

        public PrintItem()
        {
            //for reflection
        }

        public PrintItem(PrintItemDTO item)
        {
            Name = item.Name;
            Amount = item.Amount;
            IsExempt = item.IsExempt;
        }

        public PrintItem(string name, decimal amount, bool isExempt)
        {
            Name = name;
            Amount = Amount;
            IsExempt = isExempt;
        }

        public decimal CalculateTotalAmount()
        {
            TotalAmount = Amount + SalesTaxAmount + MarginAmount;
            return TotalAmount;  //maybe do not have to return this. Revisit
        }
    }
}
