﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.Printer.Models
{
    public class PrintItemDTO
    {
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public bool IsExempt { get; set; }
    }
}
