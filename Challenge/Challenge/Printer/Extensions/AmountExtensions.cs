﻿using System;

namespace Challenge.Printer.Extensions
{
    public static class AmountExtensions
    {
        //rounds to even cent: 0.97 ==> 0.96
        public static decimal RoundToEvenCent(this decimal amount)
        {
            return Math.Round(amount / 0.02m, 0) * 0.02m;
        }

        //rounds to even cent: 0.973 ==> 0.97
        public static decimal RoundToCent(this decimal amount)
        {
            return Math.Round(amount / 0.01m, 0) * 0.01m;
        }
    }
}
