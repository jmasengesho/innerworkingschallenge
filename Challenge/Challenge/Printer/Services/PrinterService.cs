﻿using System;
using System.Collections.Generic;
using System.Text;
using Challenge.Printer.Models;

namespace Challenge.Printer.Services
{

    //Due to time constraints i wrote tests for one service but all the others can be tested the same way
    public class PrinterService : IPrinterService
    {
        public IPrinterJob PrinterJob { get; set; }   //can only process one job at time, for scalability, you can launch as many of this service as you wish
        public ISalesTaxCalculator SalesTaxCalculator { get; set; }
        public IMarginAmountCalculator MarginAmountCalculator { get; set; }
        public IInvoiceService InvoicePrinter { get; set; }


        public PrinterService(IInvoiceService outputProcessor, ISalesTaxCalculator salesTaxCalculator, IMarginAmountCalculator marginAmountCalculator)
        {
            SalesTaxCalculator = salesTaxCalculator;
            MarginAmountCalculator = marginAmountCalculator;
            InvoicePrinter = outputProcessor;
        }

        public void LoadJob(IPrinterJob job)
        {
            if(job.PrintItems.Count == 0 || job.PrintItems == null)
            {
                throw new Exception("The job is empty. Please enter job items");
            }

            PrinterJob = job;  
        }

        public void Process()   //calculate job individual item amounts and totals
        {
            foreach (var item in PrinterJob.PrintItems)
            {
                item.SalesTaxAmount = SalesTaxCalculator.Calculate(item.Amount, item.IsExempt);  
                item.MarginAmount = MarginAmountCalculator.Calculate(item.Amount, PrinterJob.ExtraMarginRatio);
                item.CalculateTotalAmount();
                PrinterJob.TotalAmount += item.TotalAmount;
            }
        }

        public string PrintInvoice()  //not responsible for printing but uses an invoice printer as a dependency
        {
            return InvoicePrinter.CreateJobInvoice(PrinterJob);
        }

        public string PrintJobItems() //not responsible for printing but uses an invoice printer as a dependency
        {
            return InvoicePrinter.PrintJobInputDetails(PrinterJob);
        }

        public string PrintFullInvoice(IPrinterJob job) //not responsible for printing but uses an invoice printer as a dependency
        {
            return InvoicePrinter.PrintFullInvoice(PrinterJob);
        }
    }
}
