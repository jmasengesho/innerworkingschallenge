﻿using Challenge.Printer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.Printer.Services
{
    public interface IInvoiceService
    {
        string CreateJobInvoice(IPrinterJob job);
        string PrintJobInputDetails(IPrinterJob job);
        string PrintFullInvoice(IPrinterJob job);
    }
}
