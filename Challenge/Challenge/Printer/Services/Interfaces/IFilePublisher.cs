﻿using Challenge.Printer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.Printer.Services.Interfaces
{
    public interface IFilePublisher
    {
        string JobsFilePath { get; set; }  //the test file is included in the solution. Could make it configurable
        string InvoiceFilePath { get; set; }  //set from program.cs. Could be anywhere on your computer

        IList<PrinterJobDTO> LoadJobs();
        void SetOutFileLocation(string path);    //have to specify for now. Can make it remote
        bool PublishInvoice(string invoice);

    }
}
