﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.Printer.Services
{
    public interface IMarginAmountCalculator
    {
        decimal Calculate(decimal amount, decimal extraMargin);
    }
}
