﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.Printer.Services
{
    public interface ISalesTaxCalculator
    {
        decimal Calculate(decimal amount, bool isExempt);
    }
}
