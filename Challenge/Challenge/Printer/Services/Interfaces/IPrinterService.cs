﻿using Challenge.Printer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.Printer.Services
{
    interface IPrinterService
    {
        void LoadJob(IPrinterJob job);
        void  Process();
        string PrintInvoice();
        string PrintJobItems();
        string PrintFullInvoice(IPrinterJob job);

    }
}
