﻿using System;
using System.Collections.Generic;
using System.Text;
using Challenge.Printer.Extensions;
using Challenge.Printer.Models;
using Challenge.Printer.Services.Interfaces;

namespace Challenge.Printer.Services
{
    public class InvoiceService : IInvoiceService
    {
        public IFilePublisher  FilePublisher { get; set; }  //to be injected in via the method to write to file, not needed in the constructor

        public InvoiceService()
        {
            //for reflection
        }

        public string CreateJobInvoice(IPrinterJob job)
        {
            var output = new StringBuilder();

            //add individual lines
            foreach(var item in job.PrintItems)
            {
                output.AppendLine($@"{item.Name}: ${(item.Amount + item.SalesTaxAmount).RoundToCent()}");  //uses a decimal extensions to round
            }

            //add total
            output.AppendLine($@"total: ${job.TotalAmount.RoundToEvenCent()}");  //uses a decimal extensions to round

            return output.ToString();
        }     

        public string PrintJobInputDetails(IPrinterJob job)
        {
            var output = new StringBuilder();

            //add individual lines
            foreach (var item in job.PrintItems)
            {
                output.AppendLine($@"{item.Name} ${item.Amount}");
            }

            return output.ToString();
        }

        public string PrintFullInvoice(IPrinterJob job)  //for concole log and file output
        {
            var output = new StringBuilder();

            //print job id
            output.AppendLine($@"{job.JobId}:");

            //print input
            output.AppendLine(PrintJobInputDetails(job));

            //print output 

            output.AppendLine("Output:");

            output.AppendLine(CreateJobInvoice(job));

            //add a separator 
            output.AppendLine("==========================");

            return output.ToString();
        }


    }
}
