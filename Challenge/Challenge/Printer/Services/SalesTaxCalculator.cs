﻿using Challenge.Printer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.Printer.Services
{
    public class SalesTaxCalculator : ISalesTaxCalculator
    {
        public decimal SalesTaxRatio { get; set; } = 0.07m; //can be set by a setter or through configuration

        public SalesTaxCalculator()
        {
        }

        public decimal Calculate(decimal amount, bool isExempt)
        {
            return isExempt ? 0.00m : amount * SalesTaxRatio;
        }
    }
}
