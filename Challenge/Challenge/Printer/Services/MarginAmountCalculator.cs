﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.Printer.Services
{
    public class MarginAmountCalculator : IMarginAmountCalculator
    {
        public decimal MarginRatio { get; set; } = 0.11m; //can be set by a setter or through configuration

        public decimal Calculate(decimal amount, decimal extraMargin = 0.00m)  //investigate using an override if there are many variations of this
        {
            return amount * (MarginRatio + extraMargin);  
        }
    }
}
