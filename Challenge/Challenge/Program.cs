﻿using Challenge.Data;
using Challenge.Printer.Models;
using Challenge.Printer.Services;
using Challenge.Printer.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace Challenge
{
    class Program
    {
        private static IServiceProvider _serviceProvider;

        static void Main(string[] args)
        {
            //register all necessary services using the built in IoC. Could have used Autofac. 
            RegisterServices();
          
            //request needed services
            var printerService = _serviceProvider.GetService<IPrinterService>();



            //UNCOMMENT TO READ AND WRITE INVOICES FROM AND TO A FILE. NOTE: Run this on Windows. There was an issue with File on Mac
            //var fileProcessor = _serviceProvider.GetService<IFilePublisher>();
            ////read and parse all jobs
            //var jobs = fileProcessor.LoadJobs();
            ////set the location for the output
            //fileProcessor.SetOutFileLocation(@"C:\Users\jmasengesho\Documents\Challenge\Challenge\Challenge\Data\Invoices\" + "InvoiceOutput" + ".txt");  //TODO: You need to set this location on your local machine

            //var printJobs = new List<IPrinterJob>();
            //foreach (var job in jobs)
            //{
            //    printJobs.Add(new PrinterJob(job));
            //}

            ////loop through all jobs and print them
            //foreach (var job in printJobs)
            //{
            //    printerService.LoadJob(job);
            //    printerService.Process();
            //    var jobOutput = printerService.PrintFullInvoice(job);

            //    fileProcessor.PublishInvoice(jobOutput);
            //    Console.WriteLine($@"{job.JobId} has been written to {fileProcessor.InvoiceFilePath}");
            //}

            //UNCOMMENT TO RUN THE APPLICATION WITH HARDCODED TEST SCENARIOS. NOTE: Tested on Windows and Mac
            //Job: 1
            var job1PrinterItems = new List<IPrintItem> { new PrintItem { Name = "envelopes", Amount = 520.00m, IsExempt = false },
                                                         new PrintItem { Name = "letterhead", Amount = 1983.37m, IsExempt = true} };

            var printJob1 = new PrinterJob(job1PrinterItems, 0.05m);

            printerService.LoadJob(printJob1);
            printerService.Process();
            var job1Output = printerService.PrintInvoice();
            Console.WriteLine("Job 1:");
            Console.WriteLine(printerService.PrintJobItems());
            Console.WriteLine("Output:");
            Console.WriteLine(job1Output);
            Console.WriteLine("==========================");


            //Job 2:
            var job2PrinterItems = new List<IPrintItem> { new PrintItem { Name = "t-shirts", Amount = 294.04m, IsExempt = false } };

            var printJob2 = new PrinterJob(job2PrinterItems, 0.00m);

            printerService.LoadJob(printJob2);
            printerService.Process();
            var job2Output = printerService.PrintInvoice();
            Console.WriteLine("Job 2:");
            Console.WriteLine(printerService.PrintJobItems());
            Console.WriteLine("Output:");
            Console.WriteLine(job2Output);
            Console.WriteLine("==========================");

            //Job 3: 
            var job3PrinterItems = new List<IPrintItem> { new PrintItem { Name = "frisbees", Amount = 19385.38m, IsExempt = true },
                                                        new PrintItem {  Name = "yo-yos", Amount = 1829, IsExempt = true} };

            var printJob3 = new PrinterJob(job3PrinterItems, 0.05m);

            printerService.LoadJob(printJob3);
            printerService.Process();
            var job3Output = printerService.PrintInvoice();
            Console.WriteLine("Job 3:");
            Console.WriteLine(printerService.PrintJobItems());
            Console.WriteLine("Output:");
            Console.WriteLine(job3Output);
            Console.WriteLine("==========================");


            //Dispose IoC services
            DisposeServices();

            Console.WriteLine("Job Completed. Press any key to stop");
            Console.ReadKey();
            
        }


        private static void RegisterServices()
        {
            //used the built in IoC but we can easily add Autofac or NInject
            var collection = new ServiceCollection();
            collection.AddScoped<IPrinterService, PrinterService>();
            collection.AddScoped<IInvoiceService, InvoiceService>();
            collection.AddScoped<ISalesTaxCalculator, SalesTaxCalculator>();
            collection.AddScoped<IMarginAmountCalculator, MarginAmountCalculator>();
            collection.AddScoped<IFilePublisher, FilePublisher>();

            _serviceProvider = collection.BuildServiceProvider();
        }

        private static void DisposeServices()
        {
            if (_serviceProvider == null)
            {
                return;
            }
            if (_serviceProvider is IDisposable)
            {
                ((IDisposable)_serviceProvider).Dispose();
            }
        }
    }
}
