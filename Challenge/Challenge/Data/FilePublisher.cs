﻿using Challenge.Printer.Models;
using Challenge.Printer.Services.Interfaces;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace Challenge.Data
{
    public class FilePublisher : IFilePublisher
    {
        public string JobsFilePath { get; set; }  //the test file is included in the solution. Could make it configurable
        public string InvoiceFilePath { get; set; }  //set from program.cs. Could be anywhere on your computer

        public FilePublisher()
        {
            //for reflection
        }

        public IList<PrinterJobDTO> LoadJobs()
        {
            //load json content as string
            var jsonStringContent = ReadJobs();

            //deserialize json to dtos
            var jobs = JsonConvert.DeserializeObject<List<PrinterJobDTO>>(jsonStringContent);

            return jobs;
        }

        public bool PublishInvoice(string output)
        {
            //if the file exist, delete it and recreate it
            try
            {
                if (File.Exists(InvoiceFilePath))
                {
                    File.AppendAllText(InvoiceFilePath, output);
                }
                else
                {
                    File.WriteAllText(InvoiceFilePath, output);
                }            
            }
            catch(FileNotFoundException e)
            {
                throw e;
            }        

            return true;
        }

        private string ReadJobs()  //read all jobs from a json file
        {
            string[] files = Directory.GetFiles(@".\Data\Jobs", "*.txt");
            JobsFilePath = Path.GetFullPath(files[0]);

            string stringContent;
            using (var r = new StreamReader(JobsFilePath))
            {
                stringContent = r.ReadToEnd();
            }

            return stringContent;
        }

        public void SetOutFileLocation(string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }

            InvoiceFilePath = path;
        }
    }
}
