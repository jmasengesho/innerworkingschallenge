

What I built:
-----------------

I built an .net core console app that can process jobs (calculations), process invoices (printing), but also it can read and write output to a "local" file.

How to set up:
----------------

1) Make sure that you have .net core (2.1) installed.
2) Clone this repo from Bitbucket in your preferred folder: "git clone https://jmasengesho@bitbucket.org/jmasengesho/innerworkingschallenge.git"
3) Restore the project dependencies if asked to.
4) Make sure you can build successfully.

How to Run:
---------------

a) By the default, the test jobs are hardcoded in Program.cs's main Function. Just run the application and see expected output in the console.
b) If you wish to read jobs and write out to a file do the following:
     -- Investigate the file under: Challenge\Data\Jobs\PrinterJobs.txt. The jobs are stored as a json object
     -- In Program.cs, uncomment lines from 26-47
	 -- On line 30: Set the preferred output location. Change the folder and the file name if you wish
	 -- Run the code again, the console will tell you that all the jobs were written. 

How to Run Test:
-----------------------

- In the same solution, go under ChallengeTest project
- Open PrinterTests.cs
- From Test Explorer, Click Run All. 




Details about the Artchitecture
--------------------------------

Folder Structure
-----------------
Challenge
  Data : Test Data and FilePublisher concrete class
  Printer
    Extensions : Extensions for rounding
	Models : All the dodels
	Services : All the services
	  Interfaces
ChallengeTest
  ChallengeTests
  
  
 Services
 ------------
 IPrinterService/PrinterService : Main Service, coordinates job totals calculations and invoice printing
 IInvoiceService/InvoiceService : Given a job that has been processed, it prints the output for Console or File output
 IMarginAmountCalculator/MarginAmountCalculator: Responsible for calculating margin amounts for a print item provided to it
 ISalesTaxCalculator/SalesTaxCalculator: Responsible for calculating sales tax amounts for a print item provided to it
 IFilePublisher/FilePublisher: Responsible for reading and writing to files (reading jobs and writing invoices)
 
 
Inversion of Control (IoC)
-----------------------------

--Opted to use the built-in .net core IoC because of simplicity but could use Autofac or NInject
--In Program.cs 's RegisterServices, all the services are registered 
--In Main method, all the necessary services are requested 

Testing
--------
--It could have been better to test all the services but due to time constraints, i tested PrinterService with provided program statement input. I showed how Exceptiongs can be tested.
--Ideally I would've used Mock, but because of code simplicity, I used Fakes instead.


Test Data:
---------------------
Job 1:
extra-margin
envelopes 520.00
letterhead 1983.37 exempt

should output:
envelopes: $556.40
letterhead: $1983.37
total: $2940.30

Job 2:
t-shirts 294.04

output:
t-shirts: $314.62
total: $346.96

Job 3:
extra-margin
frisbees 19385.38 exempt
yo-yos 1829 exempt

output:
frisbees: $19385.38
yo-yos: $1829.00
total: $24608.68

  
